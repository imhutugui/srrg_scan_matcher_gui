# srrg_scan_matcher_gui

This package contains the basic viewers for scan matching and 2D pose tracking.

## Prerequisites

This package requires:
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_boss](https://gitlab.com/srrg-software/srrg_boss)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_viewers](https://gitlab.com/srrg-software/srrg_core_viewers)
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers)

## Applications

* `cloud_viewer_app`: displays clouds saved in `.dat` format.