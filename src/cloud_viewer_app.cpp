#include "cloud2d_viewer.h"
#include <QApplication>
#include <fstream>
#include <iostream>
#include <sstream>
#include "cloud2d.h"

#include <QFileDialog>

using namespace std;
using namespace srrg_scan_matcher_gui;
using namespace srrg_scan_matcher;

Cloud2DWithTrajectory* loadCloud(const char* filename) {
    ifstream __is (filename);
    if (! __is.good())
      return 0;
    Cloud2DWithTrajectory * c=new Cloud2DWithTrajectory;
    if (! c->load(__is)) {
      delete c;
      return 0;
    }
    return c;
}

int main(int argc, char** argv) {

  QApplication app(argc,argv);
  Cloud2DViewer viewer;
  viewer.show();
  

  std::cout << "Press L: load clouds, C: clear clouds, Q: close app" << std::endl;
  while (true){
    app.processEvents();
    QKeyEvent* event=viewer.lastKeyEvent();
    if (event){
      if (event->key() == Qt::Key_L){
	QFileDialog dialog;
	dialog.setFileMode(QFileDialog::ExistingFiles);
	dialog.setNameFilter("*cloud*.dat");

	if(dialog.exec()) {
	  QStringList filesSelected = dialog.selectedFiles();
	  for (int filenumber = 0; filenumber < filesSelected.size(); filenumber++){
	    std::string filename = filesSelected[filenumber].toStdString();
	    std::cerr << "Loading: " << filename << std::endl;
	    Cloud2DWithTrajectory* c=loadCloud(filename.c_str());
	    std::cerr << "Pose: " << srrg_core::t2v(c->pose()).transpose() << std::endl;

	    if (c){
	      std::cerr << "Adding cloud to viewer." << std::endl;
	      
	      Eigen::Vector3f color(1, 0, 0);
	      c->setColor(color);
	      viewer.clouds.push_back(c);
	    }
	  }
	}
      } else if (event->key() == Qt::Key_C){
	for (size_t c = 0; c < viewer.clouds.size(); c++)
	  delete viewer.clouds[c];
	viewer.clouds.clear();
	std::cerr << "Clouds cleared." << std::endl;
      } else if (event->key() == Qt::Key_Q) {
	std::cerr << "Quitting..." << std::endl;
	break;
      }

      viewer.keyEventProcessed();
    }
    usleep(10000);
  }
  
}
