#include "cloud2d_viewer.h"
namespace srrg_scan_matcher_gui {
  using namespace std;
  using namespace Eigen;
  using namespace srrg_scan_matcher;

  Cloud2DViewer::Cloud2DViewer(){
    _draw_normals = true;
  }
  
  void Cloud2DViewer::draw(){
    for (size_t i = 0; i<clouds.size(); i++){
      Cloud2DWithTrajectory* cloudTrajectory = dynamic_cast<Cloud2DWithTrajectory*>(clouds[i]);
      if (cloudTrajectory)
	cloudTrajectory->draw(_draw_normals);
      else{
	Cloud2DWithPose* cloud=clouds[i];
	cloud->draw(_draw_normals, true);
      }
    }
  }


  void Cloud2DViewer::clear(){
    clouds.clear();
    updateGL();
  }
}
