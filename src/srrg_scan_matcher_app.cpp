//scan matcher things
#include "projective_correspondence_finder2d.h"
#include "laser_message_tracker.h"

//srrg core
#include "srrg_messages/base_sensor_message.h"
#include "srrg_messages/message_reader.h"
#include "srrg_system_utils/system_utils.h"


//interface
#include <QApplication>
#include "tracker_viewer.h"

using namespace std;
using namespace srrg_core;
using namespace srrg_scan_matcher;
using namespace srrg_scan_matcher_gui;

double inlier_distance;
int iterations;
int frame_skip;
double bpr;
double min_correspondences_ratio;
double local_map_clipping_range;
double local_map_clipping_translation_threshold;
double max_matching_range;
double min_matching_range;
int num_matching_beams;
double merging_distance, merging_normal_angle;
double voxelize_res;
double matching_fov;

bool use_gui;
bool projective_merge;
std::string dump;

double tracker_damping;
std::string cfinder;
bool verbose;

double laser_translation_threshold;
double laser_rotation_threshold;

std::vector<int> odom_weights;
std::string dumpFilename;

const char* banner[] = {
  "srrg_scan_matcher_app: provides a local 2D tracker from a log in txtio format",
  "",
  "Usage: srrg_scan_matcher_app <options> <input_dump_file>",
  "Options:",
  "-inlier_distance <float>:                          tracker inlier distance",
  "-iterations <int>:                                 tracker iterations",
  "-frame_skip <int>:                                 use one scan every <frame_skip> scans",
  "-bpr <float>:                                      tracker bad points ratio",
  "-min_correspondences_ratio <float>:                tracker minimum correspondences ratio",
  "-local_map_clipping_range <float>:                 tracker local map clipping range",
  "-local_map_clipping_translation_threshold <float>: tracker local map clipping translation threshold",
  "-max_matching_range <float>:                       tracker max matching range",
  "-min_matching_range <float>:                       tracker min matching range",
  "-num_matching_beams <int>:                         tracker num matching beams",
  "-merging_distance <float>:                         tracker merging distance",
  "-merging_normal_angle <float>:                     tracker merging normal angle",
  "-voxelize_res <float>:                             tracker voxelization resolution",
  "-matching_fov <float>:                             tracker matching field of view",
  "-tracker_damping <float>:                          tracker damping factor",
  "-laser_translation_threshold <float>:              translation threshold to process a new laser scan (0=process all)",
  "-laser_rotation_threshold <float>:                 orientation threshold to process a new laser scan (0=process all)",
  "-use_gui:                                          displays the GUI",
  "-projective_merge:                                 use projective merge",
  "-verbose:                                          display execution info",
  "-odom_weights <int,int,int>:                       weights for odometry (x,y,theta) separated by commas without spaces (e.g: 10,5,10) ",
  "-cfinder <string>:                                 choose between projective or nn correspondence finder",
  "-dump <string>:                                    filename to save the dumps generated",
  "-h:                                                shows this help",
  0
};


void readParameters(int argc, char **argv){
  if (argc<2) {
    std::cerr << std::endl << "srrg_scan_matcher_app:  provides a local 2D tracker from a log in txtio format" << std::endl;
    std::cerr << "Error: you must provide some parameter. Use 'srrg_scan_matcher_app -h' for help. " << std::endl;
    exit(0);
  }

  //Default values
  inlier_distance = 0.3;
  iterations = 10;
  frame_skip = 1;
  bpr = 0.4;
  min_correspondences_ratio = 0.5;
  local_map_clipping_range = 10.0;
  local_map_clipping_translation_threshold = 5.0;
  max_matching_range = 0.0;
  min_matching_range = 0.0;
  num_matching_beams = 0;
  merging_distance = 0.2;
  merging_normal_angle = 1.0;
  voxelize_res = 0.0;
  matching_fov = 0.0;
  tracker_damping = 1.0;
  laser_translation_threshold = 0.0;
  laser_rotation_threshold = 0.0;
  use_gui = false;
  projective_merge = false;
  verbose = false;
  odom_weights.resize(3);
  odom_weights[0] = 100;
  odom_weights[1] = 50;
  odom_weights[2] = 100;
  cfinder = std::string("projective");

  bool have_dumpFilename = false;

  //parsing program options
  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      exit(0);
    } else if (!strcmp(argv[c], "-inlier_distance")) {
      inlier_distance = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-iterations")) {
      iterations = atoi(argv[++c]);
    } else if (!strcmp(argv[c], "-frame_skip")) {
      frame_skip = atoi(argv[++c]);
    } else if (!strcmp(argv[c], "-bpr")) {
      bpr = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-min_correspondences_ratio")) {
      min_correspondences_ratio = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-local_map_clipping_range")) {
      local_map_clipping_range = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-local_map_clipping_translation_threshold")) {
      local_map_clipping_translation_threshold = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-max_matching_range")) {
      max_matching_range = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-min_matching_range")) {
      min_matching_range = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-num_matching_beams")) {
      num_matching_beams = atoi(argv[++c]);
    } else if (!strcmp(argv[c], "-merging_distance")) {
      merging_distance = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-merging_normal_angle")) {
      merging_normal_angle = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-voxelize_res")) {
      voxelize_res = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-matching_fov")) {
      matching_fov = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-tracker_damping")) {
      tracker_damping = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-laser_translation_threshold")) {
      laser_translation_threshold = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-laser_rotation_threshold")) {
      laser_rotation_threshold = atof(argv[++c]);
    } else if (!strcmp(argv[c], "-odom_weights")) {
      char *str = argv[++c];
      char *pch;
      pch = strtok (str, ",");
      size_t i = 0;
      while (pch != NULL && i < odom_weights.size()) {
	odom_weights[i] = atoi(pch);
	pch = strtok (NULL, ",");
	i++;
      }
      if (pch != NULL || i < odom_weights.size()) {
	std::cerr << "Wrong odom_weights size. Type -h for help." << std::endl;
	exit(0);
      }
    } else if (!strcmp(argv[c], "-use_gui")) {
      use_gui = true;
    } else if (!strcmp(argv[c], "-projective_merge")) {
      projective_merge = true;
    } else if (!strcmp(argv[c], "-verbose")) {
      verbose = true;
    } else if (!strcmp(argv[c], "-cfinder")) {
      cfinder = std::string(argv[++c]);
    } else if (!strcmp(argv[c], "-dump")) {
      dump = std::string(argv[++c]);
    } else if (c == argc-1) {
      dumpFilename = std::string(argv[c]);
      have_dumpFilename = true;
    } else {
      std::cerr << "Unknown program option: " << argv[c] << std::endl;
      exit(0);
    }
    c++;
  }
  
  if (frame_skip <= 0)
    frame_skip = 1;

  std::cout << std::endl;
  
  std::cout << "Launched with params: " << std::endl;
  std::cout << "Log filename: " << dumpFilename << std::endl;
  std::cout << "inlier_distance " << inlier_distance << std::endl;
  std::cout << "iterations " << iterations << std::endl;
  std::cout << "frame_skip " << frame_skip << std::endl;
  std::cout << "bpr " << bpr << std::endl;
  std::cout << "min_correspondences_ratio " << min_correspondences_ratio << std::endl;
  std::cout << "local_map_clipping_range " << local_map_clipping_range << std::endl;
  std::cout << "local_map_clipping_translation_threshold " << local_map_clipping_translation_threshold << std::endl;
  std::cout << "max_matching_range " << max_matching_range << std::endl;
  std::cout << "min_matching_range " << min_matching_range << std::endl;
  std::cout << "num_matching_beams " << num_matching_beams << std::endl;
  std::cout << "merging_distance " << merging_distance << std::endl;
  std::cout << "merging_normal_angle " << merging_normal_angle << std::endl;
  std::cout << "voxelize_res " << voxelize_res << std::endl;
  std::cout << "matching_fov " << matching_fov << std::endl;
  std::cout << "use_gui " << (use_gui?"true":"false") << std::endl;
  std::cout << "projective_merge " << (projective_merge?"true":"false") << std::endl;
  std::cout << "dump " << dump << std::endl;
  std::cout << "tracker_damping " << tracker_damping << std::endl;
  std::cout << "cfinder " << cfinder << std::endl;
  std::cout << "verbose " << verbose << std::endl;

  std::cout << "laser_translation_threshold " << laser_translation_threshold << std::endl;
  std::cout << "laser_rotation_threshold " << laser_rotation_threshold << std::endl;

  std::cout << "odom_weights ";
  for (size_t i=0; i< odom_weights.size(); i++){
    std::cout << odom_weights[i];
    if (i < odom_weights.size()-1)
      std::cout << ",";
  }
  std::cout << std::endl;

  fflush(stdout);

  if (!have_dumpFilename){
    std::cout << std::endl;
    std::cout << "You must provide a last parameter with the log filename" << std::endl;
    exit(0);
  }
}

Projector2D* projector = 0;
LaserMessageTracker* lmtracker = new LaserMessageTracker(); 
ProjectiveCorrespondenceFinder2D* projective_finder = new ProjectiveCorrespondenceFinder2D;
NNCorrespondenceFinder2D* nn_finder = new NNCorrespondenceFinder2D;

int main(int argc, char **argv){

  readParameters(argc,argv);

  // Setting input parameters
  if (cfinder=="projective")
    lmtracker->setCorrespondenceFinder(projective_finder);
  else if (cfinder=="nn")
    lmtracker->setCorrespondenceFinder(nn_finder);
  else {
    std::cerr << "unknown correspondence finder type: [" << cfinder << "]" << std::endl;
    return -1;
  }

  lmtracker->setInlierDistance(inlier_distance);
  lmtracker->setIterations(iterations);
  lmtracker->setBpr(bpr);
  lmtracker->setMinCorrespondencesRatio(min_correspondences_ratio);
  lmtracker->setLocalMapClippingRange(local_map_clipping_range);
  lmtracker->setClipTranslationThreshold(local_map_clipping_translation_threshold);
  lmtracker->setMergingDistance(merging_distance);
  lmtracker->setMergingNormalAngle(merging_normal_angle);
  lmtracker->setVoxelizeResolution(voxelize_res);
  lmtracker->setEnableProjectiveMerge(projective_merge);
  lmtracker->setDumpFilename(dump);
  lmtracker->solver()->setDamping(tracker_damping);
  lmtracker->setVerbose(verbose);

  lmtracker->setFrameSkip(frame_skip);
  
  lmtracker->setMaxMatchingRange(max_matching_range);
  lmtracker->setMinMatchingRange(min_matching_range);
  lmtracker->setNumMatchingBeams(num_matching_beams);
  lmtracker->setMatchingFov(matching_fov);
  lmtracker->setOdomWeights(odom_weights[0], odom_weights[1], odom_weights[2]);
  lmtracker->setLaserTranslationThreshold(laser_translation_threshold);
  lmtracker->setLaserRotationThreshold(laser_rotation_threshold);

  std::cerr << "Tracker2D allocated" << std::endl;

  QApplication* app=0;
  Tracker2DViewer* tviewer=0;
  if (use_gui) {
    app=new QApplication(argc, argv);
    tviewer=new Tracker2DViewer(lmtracker);
    tviewer->init();
    tviewer->show();
  }
  
  MessageReader reader;
  reader.open(dumpFilename);
  if (!reader.good()){
    std::cerr << "Failed openning file: " << dumpFilename << std::endl;
    return -1;
  }

  BaseMessage* msg=0;
  while (msg = reader.readMessage()) {
    LaserMessage* laser_msg = dynamic_cast<LaserMessage*>(msg);
    lmtracker->compute(laser_msg);
  
    if (use_gui) {
      tviewer->updateGL();
      app->processEvents();
      QKeyEvent* event=tviewer->lastKeyEvent();
      if (event) {
	if (event->key()==Qt::Key_F) {
	  tviewer->setFollowRobotEnabled(!tviewer->followRobotEnabled());
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_B) {
	  tviewer->setDrawRobotEnabled(!tviewer->drawRobotEnabled());
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_R) {
	  lmtracker->reset();
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_H) {
	  std::cerr << "HELP" << std::endl;
	  std::cerr << "R: reset tracker" << std::endl;
	  std::cerr << "B: draws robot" << std::endl;
	  std::cerr << "F: enables/disables robot following" << std::endl;
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_P) {//PAUSE
	  tviewer->keyEventProcessed();
	  std::cerr << "PAUSED. Press key 'P' to continue." << std::endl;
	  while (true){
	    app->processEvents();
	    event=tviewer->lastKeyEvent();
	    if (event && event->key()==Qt::Key_P){
	      tviewer->keyEventProcessed();
	      break;
	    }
	    usleep(100000);
	  }
	}
      }
    }
  }
}
