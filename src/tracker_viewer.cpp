#include "tracker_viewer.h"

namespace srrg_scan_matcher_gui {
  using namespace std;
  using namespace Eigen;
  using namespace srrg_scan_matcher;

  Tracker2DViewer::Tracker2DViewer(Tracker2D* tracker_) {
    _tracker=tracker_;
    _follow_robot_enabled=true;
    _draw_robot_enabled=true;
  }

  void Tracker2DViewer::draw(){
    if (!_tracker)
      return;

    if (_follow_robot_enabled) {
      qglviewer::Vec vec=camera()->position();
      vec.x=_tracker->globalT().translation().x();
      vec.y=_tracker->globalT().translation().y();
      camera()->setPosition(vec);
      vec.z=0;
      camera()->lookAt(vec);
    }

    Eigen::Vector3f t=srrg_core::t2v(_tracker->globalT());
    glPushMatrix();
    glTranslatef(t.x(), t.y(), 0);
    glRotatef(180.0f/M_PI*t.z(), 0,0,1);


    if (_draw_robot_enabled) {
      glPushAttrib(GL_POINT_SIZE|GL_COLOR);
      glColor3f(0,0,0);
      glBegin(GL_LINE_STRIP);
      float l=0.25, w=0.25;
      glNormal3f(0,0,1);
      glVertex3f(0,0,0);
      glVertex3f(l,0,0);
      glVertex3f(l,w,0);
      glVertex3f(-l,w,0);
      glVertex3f(-l,-w,0);
      glVertex3f(l,-w,0);
      glVertex3f(l,0,0);
      glEnd();
      glPopAttrib();
    }
    glPushAttrib(GL_POINT_SIZE);
    glPointSize(1);
    if (_tracker->reference()) {
      //_tracker->reference()->draw(false);
      _tracker->reference()->draw(true);
    }

    glPointSize(3);
    if (_tracker->current()) {
      _tracker->current()->draw(true, _tracker->solver()->T().inverse(),false,false);
    }
    glPopAttrib();
    glPopMatrix();
  }

}
