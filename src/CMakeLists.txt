add_library(srrg_scan_matcher_gui_library 
 tracker_viewer.cpp tracker_viewer.h
 cloud2d_viewer.cpp cloud2d_viewer.h
 cloud2d_aligner_viewer.cpp cloud2d_aligner_viewer.h
 )

TARGET_LINK_LIBRARIES(srrg_scan_matcher_gui_library
  srrg_scan_matcher_library  
  srrg_core_viewers_library
  srrg_path_map_library
  ${QGLVIEWER_LIBRARY} 
  ${SRRG_QT_LIBRARIES} 
  ${OPENGL_gl_LIBRARY} 
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS} 
  ${catkin_LIBRARIES}
)

add_executable(cloud_viewer_app cloud_viewer_app.cpp)

TARGET_LINK_LIBRARIES(cloud_viewer_app
  srrg_scan_matcher_gui_library  
  srrg_scan_matcher_library  
  srrg_path_map_library
  ${QGLVIEWER_LIBRARY} 
  ${SRRG_QT_LIBRARIES} 
  ${OPENGL_gl_LIBRARY} 
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS}
)

add_executable(srrg_cloud2d_app srrg_cloud2d_app.cpp)

TARGET_LINK_LIBRARIES(srrg_cloud2d_app
  srrg_scan_matcher_gui_library  
  srrg_scan_matcher_library  
  srrg_path_map_library
  ${QGLVIEWER_LIBRARY} 
  ${QT_QTXML_LIBRARY} 
  ${QT_QTOPENGL_LIBRARY} 
  ${QT_QTGUI_LIBRARY} 
  ${QT_QTCORE_LIBRARY} 
  ${OPENGL_gl_LIBRARY} 
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS} 
)

add_executable(srrg_scan_matcher_app srrg_scan_matcher_app.cpp)

TARGET_LINK_LIBRARIES(srrg_scan_matcher_app
  srrg_scan_matcher_gui_library  
  srrg_scan_matcher_library  
  srrg_path_map_library
  ${QGLVIEWER_LIBRARY} 
  ${QT_QTXML_LIBRARY} 
  ${QT_QTOPENGL_LIBRARY} 
  ${QT_QTGUI_LIBRARY} 
  ${QT_QTCORE_LIBRARY} 
  ${OPENGL_gl_LIBRARY} 
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS} 
)
