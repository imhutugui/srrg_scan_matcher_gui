#include "cloud2d_aligner_viewer.h"

namespace srrg_scan_matcher_gui {
  using namespace std;
  using namespace Eigen;
  using namespace srrg_scan_matcher;
  using namespace srrg_core;

  void Cloud2DAlignerViewer::drawCorrespondences(Eigen::Isometry2f& T){
    glPushAttrib(GL_COLOR);
    glBegin(GL_LINES);
    glColor3f(0,0,1);
    for (size_t i = 0; i < _correspondences.size(); ++i) {
      int ref_idx  = _correspondences[i].first;
      int curr_idx = _correspondences[i].second;

      Eigen::Vector2f pt_curr = T*(*_current)[curr_idx].point();
      Eigen::Vector2f pt_ref = (*_reference)[ref_idx].point();

      glNormal3f(0,0,1);
      glVertex3f(pt_ref.x(), pt_ref.y(),0);
      glNormal3f(0,0,1);
      glVertex3f(pt_curr.x(), pt_curr.y(),0);
    }
    glEnd();
    glPopAttrib();
  }
  
  void Cloud2DAlignerViewer::draw(){
    if (_reference){
      _reference->draw(false, true);
    }
    if (_current){
      _current->draw(false, true);
    }
    if (_reference && _current){
      glPushMatrix();
      Eigen::Vector3f pose=t2v(_reference->pose());
      glTranslatef(pose.x(), pose.y(), 0);
      glRotatef(180.0f/M_PI*pose.z(), 0,0,1);

      Eigen::Isometry2f T = _reference->pose().inverse()*_current->pose();
      drawCorrespondences(T);
      glPopMatrix();
    }
  }

  void Cloud2DAlignerViewer::clear(){
    setReference(0);
    setCurrent(0);

    _correspondences.clear();
    
    updateGL();
  }
}
