#pragma once
#include "tracker2d.h"
#include "srrg_core_viewers/simple_viewer.h"
#include <qevent.h>

namespace srrg_scan_matcher_gui {

  class Tracker2DViewer: public srrg_core_viewers::SimpleViewer{
  public:
    //! ctor
    Tracker2DViewer(srrg_scan_matcher::Tracker2D* tracker_ = 0);
    void draw();
    inline void setTracker(srrg_scan_matcher::Tracker2D* tracker_){_tracker = tracker_;}
    inline bool followRobotEnabled() const  {return _follow_robot_enabled;}
    inline bool drawRobotEnabled() const {return _draw_robot_enabled;}
    inline void setFollowRobotEnabled(bool enable) {_follow_robot_enabled=enable;}
    inline void setDrawRobotEnabled(bool enable) {_draw_robot_enabled=enable;}
  protected:
    srrg_scan_matcher::Tracker2D* _tracker;
    bool _follow_robot_enabled;
    bool _draw_robot_enabled;
  };


}
