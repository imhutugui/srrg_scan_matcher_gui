#include "cloud2d_viewer.h"
#include "cloud2d_aligner_viewer.h"
#include "cloud2d_aligner.h"
#include "cloud2d_trajectory_merger.h"


#include <QApplication>
#include <QMainWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QFileInfo>
#include <QObject>

using namespace srrg_scan_matcher_gui;
using namespace srrg_scan_matcher;
using namespace srrg_core;

Cloud2DWithTrajectory* loadCloud(const char* filename) {
  ifstream __is (filename);
  if (! __is.good())
    return 0;
  Cloud2DWithTrajectory * c=new Cloud2DWithTrajectory;
  if (! c->load(__is)) {
    delete c;
    return 0;
  }
  return c;
}

class Cloud2DAppMainWindow: public QWidget {
public:

  Cloud2DAppMainWindow();
  void selectCloud(int id);
  void clearClouds();
  void resetClouds();
  void initCloudAligner();
  void initCloudMerger();
  void alignClouds();
  void mergeClouds();
  void pruneClouds();
  void setZero();
  
private:
  QGridLayout* glayout;
  QVBoxLayout* vlayout;
  QVBoxLayout* vlayout2;
  QHBoxLayout* hlayout;

  //Cloud 1 (reference)
  QHBoxLayout* hlayout_c1;
  QLabel* label_c1;
  QLabel* label_c1_size;
  QLineEdit* lineedit_c1;
  QPushButton* button_c1;

  //Cloud 2 (current)
  QHBoxLayout* hlayout_c2;
  QLabel* label_c2;
  QLabel* label_c2_size;
  QLineEdit* lineedit_c2;
  QPushButton* button_c2;

  //Viewers
  Cloud2DViewer* cviewer_c1;
  Cloud2DViewer* cviewer_c2;
  Cloud2DAlignerViewer* calignerviewer;
  Cloud2DViewer* cmergerviewer;
  QLabel* label_aligner;
  QLabel* label_merger;
  
  //Clouds
  Cloud2DWithTrajectory* cloud_ref;
  Cloud2DWithTrajectory* cloud_cur;
  Cloud2DWithTrajectory* cloud_aligner_ref;
  Cloud2DWithTrajectory* cloud_aligner_cur;
  Cloud2DWithTrajectory* cloud_merged;
  Eigen::Isometry2f cloud_ref_initial_pose, cloud_cur_initial_pose;

  //Bottom buttons
  QPushButton* button_clear;
  QPushButton* button_zero;
  QPushButton* button_align;
  QPushButton* button_merge;
  QPushButton* button_prune;
  QPushButton* button_reset;
  

  //Cloud Aligner and merger
  Projector2D *projector;
  Cloud2DAligner* c2DAligner;
  Cloud2DWithTrajectoryMerger* c2DMerger;
};
  
Cloud2DAppMainWindow::Cloud2DAppMainWindow(){
  
  glayout = new QGridLayout();
  vlayout = new QVBoxLayout();
  vlayout2 = new QVBoxLayout();
  hlayout = new QHBoxLayout();
  
  cviewer_c1 = new Cloud2DViewer();
  cviewer_c2 = new Cloud2DViewer();
  calignerviewer = new Cloud2DAlignerViewer();
  cmergerviewer = new Cloud2DViewer();
  label_aligner = new QLabel("Aligner View");
  label_merger = new QLabel("Merger View");

  QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
  sizePolicy.setHorizontalStretch(0);
  sizePolicy.setVerticalStretch(0);
  label_aligner->setSizePolicy(sizePolicy);
  label_merger->setSizePolicy(sizePolicy);
  
  //Individual viewers layout
  hlayout_c1 = new QHBoxLayout();
  label_c1 = new QLabel("Reference cloud  ");
  label_c1_size = new QLabel("Size cloud:  ");
  lineedit_c1 = new QLineEdit("Choose a reference cloud");
  lineedit_c1->setReadOnly(true);
  button_c1 = new QPushButton("Load");
  
  hlayout_c2 = new QHBoxLayout();
  label_c2 = new QLabel("Current cloud  ");
  label_c2_size = new QLabel("Size cloud:  ");
  lineedit_c2 = new QLineEdit("Choose a second cloud");
  lineedit_c2->setReadOnly(true);
  button_c2 = new QPushButton("Load");

  //Bottom frame
  button_clear = new QPushButton("Clear");
  button_zero  = new QPushButton("Set Zero");
  button_align = new QPushButton("Align");
  button_merge = new QPushButton("Merge");
  button_prune = new QPushButton("Prune Reference");
  button_reset = new QPushButton("Reset");
  
  setWindowTitle("srrg_cloud2d_viewer_app");
  
  hlayout_c1->addWidget(label_c1);
  hlayout_c1->addWidget(lineedit_c1);
  hlayout_c1->addWidget(button_c1);

  hlayout_c2->addWidget(label_c2);
  hlayout_c2->addWidget(lineedit_c2);
  hlayout_c2->addWidget(button_c2);

  vlayout->addWidget(cviewer_c1);
  vlayout->addItem(hlayout_c1);
  vlayout->addWidget(label_c1_size);
  vlayout->addWidget(cviewer_c2);
  vlayout->addItem(hlayout_c2);
  vlayout->addWidget(label_c2_size);

  vlayout2->addWidget(label_aligner);
  vlayout2->addWidget(calignerviewer);
  vlayout2->addWidget(label_merger);
  vlayout2->addWidget(cmergerviewer);
  
  hlayout->addWidget(button_clear);
  hlayout->addWidget(button_zero);
  hlayout->addWidget(button_align);
  hlayout->addWidget(button_merge);
  hlayout->addWidget(button_prune);
  hlayout->addWidget(button_reset);
  
  glayout->addLayout(vlayout, 0,0);
  glayout->addLayout(vlayout2, 0,1, -1, 1);
  glayout->addLayout(hlayout, 1,0);
  glayout->setColumnStretch(1, 2);

  setLayout(glayout);
  show();
  cviewer_c1->show();
  cviewer_c2->show();
  calignerviewer->show();
  cmergerviewer->drawNormals(false);
  cmergerviewer->show();
  
  QObject::connect(button_c1,    &QAbstractButton::clicked, [=]{selectCloud(1);});
  QObject::connect(button_c2,    &QAbstractButton::clicked, [=]{selectCloud(2);});
  QObject::connect(button_clear, &QAbstractButton::clicked, [=]{clearClouds();});
  QObject::connect(button_zero,  &QAbstractButton::clicked, [=]{setZero();});
  QObject::connect(button_align, &QAbstractButton::clicked, [=]{alignClouds();});
  QObject::connect(button_merge, &QAbstractButton::clicked, [=]{mergeClouds();});
  QObject::connect(button_prune, &QAbstractButton::clicked, [=]{pruneClouds();});
  QObject::connect(button_reset, &QAbstractButton::clicked, [=]{resetClouds();});

  cloud_ref = 0;
  cloud_cur = 0;
  cloud_aligner_ref = 0;
  cloud_aligner_cur = 0;
  cloud_merged = 0;
  
};

void Cloud2DAppMainWindow::selectCloud(int id){

  //Load cloud
  QFileDialog dialog;
  dialog.setFileMode(QFileDialog::ExistingFile);
  dialog.setNameFilter("*cloud*.dat");
    
  Cloud2DWithTrajectory* c = 0;
  QString cloudfilename;
  if (dialog.exec()) {
    QStringList filesSelected = dialog.selectedFiles();
    if (filesSelected.size()){
      cloudfilename = filesSelected[0];
      std::string filename = cloudfilename.toStdString();
      std::cerr << "Selected: " << filename << std::endl;
      c=loadCloud(filename.c_str());
    }
  }

  if (c){
    Cloud2DViewer* viewer;
    QLineEdit* lineedit;
    QLabel* labelsize;
    Cloud2DWithTrajectory* cloud;
    Cloud2DWithTrajectory* cloud3;
    Eigen::Vector3f color;

    if (id==1){
      viewer = cviewer_c1;
      lineedit = lineedit_c1;
      labelsize = label_c1_size;
      if (!cloud_ref)
	cloud_ref = new Cloud2DWithTrajectory;
      if (!cloud_aligner_ref){
	cloud_aligner_ref = new Cloud2DWithTrajectory;
      	calignerviewer->setReference(cloud_aligner_ref);
      }
      cloud = cloud_ref;
      cloud3 = cloud_aligner_ref;
      cloud_ref_initial_pose = c->pose();
      color = Eigen::Vector3f(1,0,0);
      std::cerr << "Reference cloud initial pose: " << t2v(cloud_ref_initial_pose).transpose() << std::endl;
    }else {
      viewer = cviewer_c2;
      lineedit = lineedit_c2;
      labelsize = label_c2_size;
      if (!cloud_cur)
	cloud_cur = new Cloud2DWithTrajectory;
      if (!cloud_aligner_cur){
	cloud_aligner_cur = new Cloud2DWithTrajectory;
	calignerviewer->setCurrent(cloud_aligner_cur);
      }
      cloud = cloud_cur;
      cloud3 = cloud_aligner_cur;
      cloud_cur_initial_pose = c->pose();
      color = Eigen::Vector3f(0,1,0);
      std::cerr << "Current cloud initial pose: " << t2v(cloud_cur_initial_pose).transpose() << std::endl;
    }

    c->setColor(color);
    *cloud = *c;
    *cloud3 = *c;
    
    //cloud shown in the small viewer is centered
    cloud->setPose(Eigen::Isometry2f::Identity());

    viewer->clouds.clear();
    viewer->clouds.push_back(cloud);
    lineedit->setText(QFileInfo(cloudfilename).fileName());
    labelsize->setText(QString("Size: %1").arg(cloud->size()));
      
    calignerviewer->correspondences().clear();
    calignerviewer->updateGL();

    cmergerviewer->clouds.clear();
    cmergerviewer->updateGL();
      
    delete c;
  }
};

void Cloud2DAppMainWindow::clearClouds(){
  cviewer_c1->clear();
  cviewer_c2->clear();
  calignerviewer->clear();
  cmergerviewer->clear();

  delete cloud_ref;
  delete cloud_cur;
  delete cloud_aligner_ref;
  delete cloud_aligner_cur;
  delete cloud_merged;

  cloud_ref = 0;
  cloud_cur = 0;
  cloud_aligner_ref = 0;
  cloud_aligner_cur = 0;
  cloud_merged = 0;
  
  lineedit_c1->setText("Choose a reference cloud");
  lineedit_c2->setText("Choose a second cloud");
  label_c1_size->setText("Size cloud:  ");
  label_c2_size->setText("Size cloud:  ");
  label_merger->setText("Merger View:");

  
};

void Cloud2DAppMainWindow::initCloudAligner(){

  //create projector of 2*M_PI 
  float fov = 2*M_PI; 
  int num_ranges = 720;
  projector = new Projector2D;
  projector->setFov(fov);
  projector->setNumRanges(num_ranges);
  
   //Init Cloud Aligner
  c2DAligner=new Cloud2DAligner(projector);
  c2DAligner->useNNCorrespondenceFinder();
  
  float inliersDistance = 0.05;
  float minInliersRatio = 0.5;
  int minNumCorrespondences = 50;
  
  c2DAligner->setInliersDistance(inliersDistance);
  c2DAligner->setMinInliersRatio(minInliersRatio);
  c2DAligner->setMinNumCorrespondences(minNumCorrespondences);
  
}

void Cloud2DAppMainWindow::initCloudMerger(){
  c2DMerger = new Cloud2DWithTrajectoryMerger;
}

void Cloud2DAppMainWindow::alignClouds(){
  if (!cloud_aligner_ref || !cloud_aligner_cur){
    std::cerr << "You must select two clouds first" << std::endl;
    return;
  }

  
  //Aligning clouds
  c2DAligner->setReference(cloud_aligner_ref);	  
  c2DAligner->setCurrent(cloud_aligner_cur);
  
  Eigen::Isometry2f initial_guess = cloud_aligner_ref->pose().inverse()*cloud_aligner_cur->pose();
  c2DAligner->compute(initial_guess);
  c2DAligner->validate();

  Eigen::Isometry2f result = c2DAligner->T();
  cloud_aligner_cur->setPose(cloud_aligner_ref->pose()*result);

  CorrespondenceVector correspondences = c2DAligner->correspondenceFinder()->correspondences();
  calignerviewer->setCorrespondences(correspondences);
  calignerviewer->updateGL();
}

void Cloud2DAppMainWindow::mergeClouds(){
  if (!cloud_aligner_ref || !cloud_aligner_cur){
    std::cerr << "You must select two clouds first" << std::endl;
    return;
  }

  //Merging clouds
  c2DMerger->setReference(cloud_aligner_ref);	  
  c2DMerger->setCurrent(cloud_aligner_cur);
  c2DMerger->compute();
  cloud_merged = c2DMerger->cloudMerged();

  std::cerr << "Merged cloud size: " << cloud_merged->size() << std::endl;
  Eigen::Vector3f color = Eigen::Vector3f(0.7,0.1,0);
  cloud_merged->setColor(color);

  label_merger->setText(QString("Merger View: %1").arg(cloud_merged->size()));
  
  cmergerviewer->clouds.clear();
  cmergerviewer->clouds.push_back(cloud_merged);
  cmergerviewer->updateGL();
}

void Cloud2DAppMainWindow::pruneClouds(){
  if (!cloud_aligner_ref || !cloud_aligner_cur){
    std::cerr << "You must select two clouds first" << std::endl;
    return;
  }

  //Merging clouds
  c2DMerger->setReference(cloud_aligner_ref);	  
  c2DMerger->setCurrent(cloud_aligner_cur);
  c2DMerger->prune();
  cloud_merged = c2DMerger->cloudMerged();

  std::cerr << "Pruned cloud size: " << cloud_merged->size() << std::endl;
  Eigen::Vector3f color = Eigen::Vector3f(0.7,0.1,0);
  cloud_merged->setColor(color);

  label_merger->setText(QString("Merger View: %1").arg(cloud_merged->size()));
  
  cmergerviewer->clouds.clear();
  cmergerviewer->clouds.push_back(cloud_merged);
  cmergerviewer->updateGL();
}

void Cloud2DAppMainWindow::resetClouds(){
  if (cloud_aligner_ref)
    cloud_aligner_ref->setPose(cloud_ref_initial_pose);
  if (cloud_aligner_cur)
    cloud_aligner_cur->setPose(cloud_cur_initial_pose);

  calignerviewer->correspondences().clear();
  calignerviewer->updateGL();
}

void Cloud2DAppMainWindow::setZero(){
  Eigen::Isometry2f initial_guess = cloud_aligner_ref->pose().inverse()*cloud_aligner_cur->pose();
  
  if (cloud_aligner_ref)
    cloud_aligner_ref->setPose(Eigen::Isometry2f::Identity());

  initial_guess.translation() = Eigen::Vector2f(0,0);
  if (cloud_aligner_cur)
    cloud_aligner_cur->setPose(initial_guess);

  calignerviewer->correspondences().clear();
  calignerviewer->updateGL();
}

int main(int argc, char** argv){

  QApplication app(argc,argv);

  Cloud2DAppMainWindow* mainWindow = new Cloud2DAppMainWindow();

  mainWindow->initCloudAligner();
  mainWindow->initCloudMerger();
  
  app.exec();
}
